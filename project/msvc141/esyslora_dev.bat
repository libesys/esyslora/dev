@echo off

set ESYS_DEV=%~dp0\..\..
set ESYSLORA_DEV=%~dp0\..\..
set MSWORD2MD_DEV=%~dp0\..\..
set MYSYSC_DEV=%~dp0\..\..
set ESYS_SYSC=%~dp0\..\..\src\esysc

set ESYSFILE=%~dp0\..\..\src\esysfile
set ESYSTEST=%~dp0\..\..\src\esystest
set PYSWIG=%~dp0\..\..\src\pyswig\src\pyswig
set MATPLOTLIB_CPP=%~dp0\..\..\extlib\matplotlib-cpp

set ESYSLORA=%~dp0\..\..\src\esyslora

set BIN32=vc141_dll
set BIN64=vc141_x64_dll
set LIB32=vc141_dll
set LIB64=vc141_x64_dll

"%ESYSSDK_INST_DIR%\bin\esyssdkcli" --call_msvc="2017" "%~dp0\esyslora_dev.sln"
