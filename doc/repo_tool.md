# Repo: The Multiple Git Repository Tool

Repo is a tool built on top of Git. Repo helps manage many Git repositories, does the uploads to
revision control systems, and automates parts of the development workflow. Repo is not meant to
replace Git, only to make it easier to work with Git. The repo command is an executable Python
script that you can put anywhere in your path.

For more information, visit [git-repo](https://gerrit.googlesource.com/git-repo/).

# Installation under Ubuntu 18.04 and later

## Using apt-get

Simply type:

```
$ sudo apt-get install repo
```

## Manually

Simply follow the steps from the [repo tool
pages](https://source.android.com/setup/build/downloading). The summary is:

```
$ mkdir ~/bin
$ curl https://storage.googleapis.com/git-repo-downloads/repo > ~/bin/repo
$ chmod a+x ~/bin/repo
```

# Installation under Windows

The easiest is to use WSL, but it’s also possible to use Cygwin.

## Using Ubuntu under WSL

Since essentially, it’s just Ubuntu, just follow what is said above about Ubuntu. To install WSL,
here is the list of steps.




### Enable WSL

* Type appwiz.cpl into Windows search and choose Run as administrator

![alt text](images/appwiz.cpl.png "The text")






* Click "Turn Windows features on or off

![alt text](images/turn_on_windows_features_arrow.png "The text")






* Turn on WSL

![alt text](images/enable_wsl_arrow.png "The text")






### Install a Linux distribution

* The only way to do this easily is to install it from the Microsoft Store. But unfortunately,
Vaisala has a very restricted amount of available apps there. So you will have to contact IT to
give you access rights to what Linux distribution you want to use, e.g. Ubuntu. You will then
received a link to click and you will see what is shown below. Of course, the difference will be
that instead of "Launch" you will have "Install" 

![alt text](images/ubuntu_install.png "The text")






* When it's all done, you can find the Ubuntu application like any other application.

![alt text](images/ubuntu_install.png "The text")



### VPN connectivity issues

For some reason, access to Vaisala git repos doesn't work over VPN. The issue is that the DNS
servers used by the VPN connection are not propagated to WSL. To fix this, do the following:

* Start a "Windows PowerShell"

* Type the following command

    * Get-DnsClientServerAddress -AddressFamily IPv4 | Select-Object -ExpandPropert ServerAddresses

* Then go to WSL command line and type

    * sudo nano /etc/resolv.conf

* Then add the 2 first IP address shown by the command run in the Windows PowerShell, make sure
that the 2 fist IP addresses are the first ones in the /etc/resolv.conf file

* To make sure the host can be authenticated properly type

    * ssh [ssh://git@git.vaisala.com](ssh://git@git.vaisala.com)

* It should work know, assuming you did configure your SSH key in WSL


## Using Cygwin

* Download the installer from [here](https://www.cygwin.com/setup-x86_64.exe)

* Install git

![alt text](images/cygwin_git_01.png "The text")






* Install gunpg

![alt text](images/cygwin_gnupg_01.png "The text")






* Install Python 2 or 3 if the version of the Repo tool supports finally Python 3. The picture
below shows the installation of Python 2

![alt text](images/cygwin_python2_01.png "The text")






* Install curl

![alt text](images/cygwin_curl.png "The text")






* [Optional] Install nano, if you want to easier tool to edit git commit and do interactive rebase

![alt text](images/cygwin_nano.png "The text")



* Install the Repo tool by typing the following (summary from [repo tool
pages](https://source.android.com/setup/build/downloading))

```
$ mkdir ~/bin
$ curl https://storage.googleapis.com/git-repo-downloads/repo > ~/bin/repo
$ chmod a+x ~/bin/repo
```






* The last step is to edit your bash profile file .bash_profile found in your Cygwin home folder
such that the ~/bin folder is in the search path: you un-comment the lines shown below. After that
simply close the current Cygwin shell and restart a new one so the edited bash profile is taken
into account.

![alt text](images/cygwin_bash_profile.png "The text")



